<p align="center">
  <a href="https://gitlab.com/atschoolprojects/malife">
    <img src="presentation/img/logo.png" height=150px alt="Project-Logo">
  </a>

  <h3 align="center">MaLife</h3>



<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
* [Roadmap](#roadmap)
* [Security](#security)
* [License](#license)
* [Contact](#contact)



<!-- ABOUT THE PROJECT -->
## About The Project
This is a Project from the M151. On the [website](https://malive.ch) (not available yet) you can write your diary online. There is no need for paper and pen anymore. Just register or log in and you will see all your entries and can add a new one. 


Here are the most important functions:
* You can create an account to get access to the MaLife online diary.
* You can read your diary entries form earlier days.
* You can create new entries and write what every you have done today.

Of course, there are hunderts of things that could be added to the site. So mabey we will add more functions in the near future.

### Built With
This website is built with:
* [PHP](https://www.php.net/)
* [HTML](https://html.com/)
* [CSS](https://html.com/css/)
* [Javascript](https://www.javascript.com)
* [Materializecss](https://materializecss.com/)

<!-- GETTING STARTED -->
## Getting Started
Perle Chat is very simple to use. Just go to the [website](https://malive.ch) (not avaiable yet). There you can log in or create a new account. Once you hav succesfully logged in, you will see all your entries. Now you can start to write what you have done today and read what happened in the past. Have fun!

### Prerequisites
There are no special prerequisites. The only thing you need is an internet connection and a browser to visit the website.

<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/mygibz/426/chat/-/issues) for a list of proposed features (and known issues).

<!-- SECURITY -->
## Security
This application is made as secure as possible with our knowlage and resources. But we can't guarantee a 100% save website.


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.


<!-- CONTACT -->
## Contact

[Manuel Gwerder](https://gitlab.com/AnotherManuel)

[Risigesan Thayakaran](https://gitlab.com/risiworks)
