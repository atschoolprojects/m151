<?php
    session_start();

    //check if the session variable is set
    if ($_SESSION["UserID"] != null) {

        //check if cancel-button was clicked
        if (isset($_POST['cancel'])) {
            //return to overview.php
            header("Location: overview.php");
        }
        else {
             //connect to the database
             $pdo = new PDO('mysql:host=localhost;dbname=malife', 'root', '');

                $statement = $pdo->prepare("SELECT * FROM `tbl_entry`");
                $statement->execute(array()); 
                
                //Checks if th requestmethod is post
                if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_POST))
                {
                    //initialise the variables
                    $entry_ID = intval($_POST["entryID"]); //intval turns variable into int
                    $content = $_POST["content"];
                    
                    //change dateformat in case of the user typed in differently
                    $date = date_format(date_create($_POST["date"]),"Y-m-d");

                    //if entryID is not null the user wants to edit the entry
                    if ($entry_ID != null) {

                        $user = $pdo->prepare("SELECT fk_user FROM tbl_entry WHERE entry_ID = ?");
                        $user->execute(array($entry_ID));

                        //Get the userID from the editing user
                        $activeUserID = $user->fetch()[0];

                        //if the editing user is the logged in user the entry become updated
                        if ($activeUserID == $_SESSION["UserID"]) {
                            $editedEntry = $pdo->prepare("UPDATE tbl_entry SET date= ?, content= ? WHERE entry_ID= ?");
                            $editedEntry->execute(array($date, $content, $entry_ID)); 
                        }
                        else {
                            header("Location: overview.php");
                        }

                       
                    }
                    else {
                        $entry = $pdo->prepare("INSERT INTO tbl_entry (fk_user, date, content)
                        VALUES (?, ?, ?)");
                        $entry->execute(array($_SESSION["UserID"], $date, $content));
                        
                    }

                    //returns to the overview page
                    header("Location: overview.php");
                } 
            } 
        }
     
     else {
        //The Sesseionvariable is not set
        header("Location: overview.php");
     }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Add Entry</title>
</head>
<body>
    
</body>
</html>